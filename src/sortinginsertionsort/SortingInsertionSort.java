
package sortinginsertionsort;

public class SortingInsertionSort {

    public static void insertionSort(int array[]) {
     int n = array.length;  
        for (int j = 1; j < n; j++) {  
            int key = array[j];  
            int i = j-1;  
            while ( (i > -1) && ( array [i] > key ) ) {  
                array [i+1] = array [i];  
                i--;  
            }  
            array[i+1] = key;  
        }  
    }  
       
    public static void main(String a[]){  
         String Identitas = "\nAmelia Lunggita Sapna/07\nFrederick Maulana Thoriq Elhaq/17\nMuhammad Radhana/27\nSafira Dwi Febrianti/37\n";
        System.out.println("Identitas : "+ Identitas);
        
        int[] arr1 = {9,14,3,2,43,11,58,22};    
        System.out.println("Sebelum Di Sorting");    
        for(int i:arr1){    
            System.out.print(i+" ");    
        }    
        System.out.println();    
            
        insertionSort(arr1);   
           
        System.out.println("Setelah Di Sorting");    
        for(int i:arr1){    
            System.out.print(i+" ");       
       
    }
    
}
}